import React, { Component } from 'react'
import { Text, View, TouchableOpacity,Image,TextInput } from 'react-native'

export default class Home extends Component {
  render() {
    return (
        <View style={{flex:1,backgroundColor:'#F4F4F4', justifyContent:'center', alignItems:'center' }}>
    
         <View><Image style={{position:'relative',top:-134, borderWidth:3, borderColor:'green', width:360,height:166 }} source={require('./MarlinSampleApp.png')}/></View>
         <View>
            <Image style={{top:-132, right:132, width:72,height:72}} source={require('./ic_ferry_intl.png')}/>
            </View>
         <View>
            <Image style={{top:-202, right:40, width:72,height:72}} source={require('./ic_ferry_domestic.png')}/>
            </View>
         <View>
            <Image style={{top:-272, left: 138, width:72,height:72}} source={require('./ic_pioneership.png')}/>
        </View>
         <View><Image style={{top:-346, left: 52, width:72,height:72}} source={require('./ic_attraction.png')}/>
         </View>
         <View>
            <TouchableOpacity style={{backgroundColor:'#213555',width:108,height:40, alignItems:'center',justifyContent:'center',}}>
                <Text style={{color:'white'}}>More......</Text></TouchableOpacity>
         </View>
        </View>
    )
  }
}
