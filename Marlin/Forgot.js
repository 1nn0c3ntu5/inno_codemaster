import React, { Component } from 'react'
import { Text, View , Image,TextInput , TouchableOpacity} from 'react-native'

export default class Forgot extends Component {
  render() {
    return (
      <View style={{flex:1,backgroundColor:'#F0F0F0', justifyContent:'center', alignItems:'center' }}>
        <View>
        <TouchableOpacity><Image style={{top:-200,right:150}} source={require('./back-button.png')}/>
        </TouchableOpacity></View> 
         <View><Image style={{top:-65,}} source={require('./Group511.png')}/></View>
         <View>
         <TextInput style={{backgroundColor:'white',color: 'black',width:273, height:43,borderWidth:1,colorWidth:'black',marginTop:25,top:-23}}placeholder='Email'>Email</TextInput>
         </View>
         <View>
          <TouchableOpacity style={{backgroundColor:'#213555', width:273, height:43, alignItems:'center', marginTop:25, top:-24, justifyContent:'center'}}>
          <Text>Request Reset</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}
