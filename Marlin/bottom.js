import React, { Component } from 'react';
import { Text, View , Image} from 'react-native';
import { Icon } from 'react-native-vector-icons/icon';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from './Home';
import App from './App';
import Register from './Register';
import Forgot from './Forgot';
import booking from './booking';
import help from './help';
import profile from './profile';

const Tab = createBottomTabNavigator();
const bottom = () => {
    return (
       <NavigationContainer>
       <Tab.Navigator screenOptions={{tabBarStyle:{height: 64, paddingHorizontal: 24, paddingVertical: 10},headerShown: false}}>
        <Tab.Screen  name='Home' component={Home} options={{
        tabBarIcon: ({color}) => (
            <Image source={require('./Home_logo.png')}/> )  }} />
        <Tab.Screen name='Booking' component={booking} options={{
        tabBarIcon: ({color}) => (
            <Image source={require('./MyBooking.png')}/> )  }} />
        <Tab.Screen name='Help' component={help} options={{
        tabBarIcon: ({color}) => (
            <Image source={require('./helpimg.png')}/> )  }} />
        <Tab.Screen name='Profile' component={profile} options={{
        tabBarIcon: ({color}) => (
            <Image source={require('./profileimg.png')}/> )  }}/>
       </Tab.Navigator>
       </NavigationContainer>
    );
};

export default bottom;