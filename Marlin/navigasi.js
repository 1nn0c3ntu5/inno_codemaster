import React, { Component } from 'react'
import { Text, View, TextInput,TouchableOpacity,Image } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'

import login from './App'
import regist from './Register'
import rumah from './Home'

const Stack = createNativeStackNavigator()

const navigasi = ( { navigation } ) => {
 
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
          name="login"
          component={login}
          options={{title: 'welcome'}} />
          <Stack.Screen
          name='Register'
          component={regist}/>
        </Stack.Navigator>
        <Stack.Screen
        name='home'
        component={rumah}/>
    </NavigationContainer>

    )
  }

export default navigasi