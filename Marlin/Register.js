import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity,TextInput } from 'react-native'

export default class Register extends Component {
  render({ navigation }) {
    return (
      <View style={{flex:1,backgroundColor:'#F0F0F0', justifyContent:'center', alignItems:'center' }}>
        <View><Image style={{top:-65,}} source={require('./Group521.png')}/></View>
        <View>
          <TextInput style={{backgroundColor:'white',color:'black', width:273, height:43,borderWidth:1,colorWidth:'black', marginTop:1, top:-20}}placeholder='Name'>Name</TextInput>
          <TextInput style={{backgroundColor:'white',color: 'black',width:273, height:43,borderWidth:1,colorWidth:'black',marginTop:25,top:-23}}placeholder='Email'>Email</TextInput>
          <TextInput style={{backgroundColor:'white',color:'black', width:273, height:43,borderWidth:1,colorWidth:'black', marginTop:27, top:-25}}placeholder='Phone'>Phone</TextInput>
          <TextInput style={{backgroundColor:'white',color: 'black',width:273, height:43,borderWidth:1,colorWidth:'black',marginTop:30,top:-28}}placeholder='password'>Password</TextInput>
        </View>
        <View>
          <TouchableOpacity style={{backgroundColor:'#213555', width:273, height:43, alignItems:'center', marginTop:25, top:-24, justifyContent:'center'}}>
          <Text>sign up</Text>
          </TouchableOpacity>
        </View>
        <View style={{backgroundColor:'white',width:360, height:35,top:137, alignItems:'center', justifyContent:'center'}}>
          <Text style={{color: 'grey'}}>already have account?
          <TouchableOpacity style={{position:'absolute',}}>
            <Text style={{color:'black',marginLeft:10,top:4}}>Sign in</Text>
            </TouchableOpacity>
            </Text>
        </View>
      </View>
    )
  }
}
